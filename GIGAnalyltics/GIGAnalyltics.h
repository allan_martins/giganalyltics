//
//  GIGAnalyltics.h
//  GIGAnalyltics
//
//  Created by Allan Martins on 15/08/17.
//  Copyright © 2017 Allan Martins. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for GIGAnalyltics.
FOUNDATION_EXPORT double GIGAnalylticsVersionNumber;

//! Project version string for GIGAnalyltics.
FOUNDATION_EXPORT const unsigned char GIGAnalylticsVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <GIGAnalyltics/PublicHeader.h>


